Test Bank Application
===========================
Knowledge requirements
--------------------------
* python 3
* sql
* gunicorn/uwsgi/supervisor/nginx
* MySQL
* strong OOD & Design patterns
* django
* sqlalchemy
* json/jsonRPC
* python packaging
* tox, pytest, coverage
* good knowledge of math!

Might be usefull
--------------------------
* memcached;
* redis;
* knowledge and desire to work with accounting information systems;

Task for level detection
--------------------------
Time to complete needed: ~4 hours

Suppose you as a developer need to create and implement system to allow couple of users to access information for his bank accounts in certain time period.

1. design and create database for the system;
2. implement system authorization with django latest on python3;
3. on protected part of site, allow user to perform actions:
    * select data for certain period of time;
    * create a chart showing bank account activity for current month via sqlalchemy integrated in  django;
    * allow user to download all of his data in zip arhived json;
4. write tests to cover 60% of the application code.

The whole project archive should be supplied (django application, database dump, README.rst with your comments on architecture).
