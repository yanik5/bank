/*
 Navicat Premium Backup

 Source Server         : bank
 Source Server Type    : PostgreSQL
 Source Server Version : 90401
 Source Host           : localhost
 Source Database       : bank_db
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90401
 File Encoding         : utf-8

 Date: 02/19/2015 11:10:00 AM
*/

-- ----------------------------
--  Sequence structure for accounts_account_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "accounts_account_id_seq" CASCADE;
CREATE SEQUENCE "accounts_account_id_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "accounts_account_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for accounts_bankuser_groups_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "accounts_bankuser_groups_id_seq" CASCADE;
CREATE SEQUENCE "accounts_bankuser_groups_id_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "accounts_bankuser_groups_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for accounts_bankuser_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "accounts_bankuser_id_seq" CASCADE;
CREATE SEQUENCE "accounts_bankuser_id_seq" INCREMENT 1 START 3 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "accounts_bankuser_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for accounts_bankuser_user_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "accounts_bankuser_user_permissions_id_seq" CASCADE;
CREATE SEQUENCE "accounts_bankuser_user_permissions_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "accounts_bankuser_user_permissions_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for accounts_transaction_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "accounts_transaction_id_seq" CASCADE;
CREATE SEQUENCE "accounts_transaction_id_seq" INCREMENT 1 START 9 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "accounts_transaction_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for auth_group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "auth_group_id_seq" CASCADE;
CREATE SEQUENCE "auth_group_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "auth_group_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for auth_group_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "auth_group_permissions_id_seq" CASCADE;
CREATE SEQUENCE "auth_group_permissions_id_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "auth_group_permissions_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for auth_permission_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "auth_permission_id_seq" CASCADE;
CREATE SEQUENCE "auth_permission_id_seq" INCREMENT 1 START 24 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "auth_permission_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for django_admin_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "django_admin_log_id_seq" CASCADE;
CREATE SEQUENCE "django_admin_log_id_seq" INCREMENT 1 START 15 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "django_admin_log_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for django_content_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "django_content_type_id_seq" CASCADE;
CREATE SEQUENCE "django_content_type_id_seq" INCREMENT 1 START 8 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "django_content_type_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Sequence structure for django_migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "django_migrations_id_seq" CASCADE;
CREATE SEQUENCE "django_migrations_id_seq" INCREMENT 1 START 5 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "django_migrations_id_seq" OWNER TO "stay-awake";

-- ----------------------------
--  Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS "django_migrations" CASCADE;
CREATE TABLE "django_migrations" (
	"id" int4 NOT NULL DEFAULT nextval('django_migrations_id_seq'::regclass),
	"app" varchar(255) NOT NULL COLLATE "default",
	"name" varchar(255) NOT NULL COLLATE "default",
	"applied" timestamp(6) WITH TIME ZONE NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "django_migrations" OWNER TO "stay-awake";

-- ----------------------------
--  Records of django_migrations
-- ----------------------------
BEGIN;
INSERT INTO "django_migrations" VALUES ('1', 'contenttypes', '0001_initial', '2015-02-19 09:34:11.406587+02');
INSERT INTO "django_migrations" VALUES ('2', 'auth', '0001_initial', '2015-02-19 09:34:11.851548+02');
INSERT INTO "django_migrations" VALUES ('3', 'accounts', '0001_initial', '2015-02-19 09:34:12.347236+02');
INSERT INTO "django_migrations" VALUES ('4', 'admin', '0001_initial', '2015-02-19 09:34:12.47934+02');
INSERT INTO "django_migrations" VALUES ('5', 'sessions', '0001_initial', '2015-02-19 09:34:12.561596+02');
COMMIT;

-- ----------------------------
--  Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS "django_content_type" CASCADE;
CREATE TABLE "django_content_type" (
	"id" int4 NOT NULL DEFAULT nextval('django_content_type_id_seq'::regclass),
	"name" varchar(100) NOT NULL COLLATE "default",
	"app_label" varchar(100) NOT NULL COLLATE "default",
	"model" varchar(100) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "django_content_type" OWNER TO "stay-awake";

-- ----------------------------
--  Records of django_content_type
-- ----------------------------
BEGIN;
INSERT INTO "django_content_type" VALUES ('1', 'log entry', 'admin', 'logentry');
INSERT INTO "django_content_type" VALUES ('2', 'permission', 'auth', 'permission');
INSERT INTO "django_content_type" VALUES ('3', 'group', 'auth', 'group');
INSERT INTO "django_content_type" VALUES ('4', 'content type', 'contenttypes', 'contenttype');
INSERT INTO "django_content_type" VALUES ('5', 'session', 'sessions', 'session');
INSERT INTO "django_content_type" VALUES ('6', 'user', 'accounts', 'bankuser');
INSERT INTO "django_content_type" VALUES ('7', 'account', 'accounts', 'account');
INSERT INTO "django_content_type" VALUES ('8', 'transaction', 'accounts', 'transaction');
COMMIT;

-- ----------------------------
--  Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS "auth_group_permissions" CASCADE;
CREATE TABLE "auth_group_permissions" (
	"id" int4 NOT NULL DEFAULT nextval('auth_group_permissions_id_seq'::regclass),
	"group_id" int4 NOT NULL,
	"permission_id" int4 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "auth_group_permissions" OWNER TO "stay-awake";

-- ----------------------------
--  Records of auth_group_permissions
-- ----------------------------
BEGIN;
INSERT INTO "auth_group_permissions" VALUES ('1', '1', '19');
INSERT INTO "auth_group_permissions" VALUES ('2', '1', '22');
COMMIT;

-- ----------------------------
--  Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS "auth_group" CASCADE;
CREATE TABLE "auth_group" (
	"id" int4 NOT NULL DEFAULT nextval('auth_group_id_seq'::regclass),
	"name" varchar(80) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "auth_group" OWNER TO "stay-awake";

-- ----------------------------
--  Records of auth_group
-- ----------------------------
BEGIN;
INSERT INTO "auth_group" VALUES ('1', 'general');
COMMIT;

-- ----------------------------
--  Table structure for accounts_bankuser_groups
-- ----------------------------
DROP TABLE IF EXISTS "accounts_bankuser_groups" CASCADE;
CREATE TABLE "accounts_bankuser_groups" (
	"id" int4 NOT NULL DEFAULT nextval('accounts_bankuser_groups_id_seq'::regclass),
	"bankuser_id" int4 NOT NULL,
	"group_id" int4 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "accounts_bankuser_groups" OWNER TO "stay-awake";

-- ----------------------------
--  Records of accounts_bankuser_groups
-- ----------------------------
BEGIN;
INSERT INTO "accounts_bankuser_groups" VALUES ('1', '2', '1');
INSERT INTO "accounts_bankuser_groups" VALUES ('2', '3', '1');
COMMIT;

-- ----------------------------
--  Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS "auth_permission" CASCADE;
CREATE TABLE "auth_permission" (
	"id" int4 NOT NULL DEFAULT nextval('auth_permission_id_seq'::regclass),
	"name" varchar(50) NOT NULL COLLATE "default",
	"content_type_id" int4 NOT NULL,
	"codename" varchar(100) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "auth_permission" OWNER TO "stay-awake";

-- ----------------------------
--  Records of auth_permission
-- ----------------------------
BEGIN;
INSERT INTO "auth_permission" VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO "auth_permission" VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO "auth_permission" VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO "auth_permission" VALUES ('4', 'Can add permission', '2', 'add_permission');
INSERT INTO "auth_permission" VALUES ('5', 'Can change permission', '2', 'change_permission');
INSERT INTO "auth_permission" VALUES ('6', 'Can delete permission', '2', 'delete_permission');
INSERT INTO "auth_permission" VALUES ('7', 'Can add group', '3', 'add_group');
INSERT INTO "auth_permission" VALUES ('8', 'Can change group', '3', 'change_group');
INSERT INTO "auth_permission" VALUES ('9', 'Can delete group', '3', 'delete_group');
INSERT INTO "auth_permission" VALUES ('10', 'Can add content type', '4', 'add_contenttype');
INSERT INTO "auth_permission" VALUES ('11', 'Can change content type', '4', 'change_contenttype');
INSERT INTO "auth_permission" VALUES ('12', 'Can delete content type', '4', 'delete_contenttype');
INSERT INTO "auth_permission" VALUES ('13', 'Can add session', '5', 'add_session');
INSERT INTO "auth_permission" VALUES ('14', 'Can change session', '5', 'change_session');
INSERT INTO "auth_permission" VALUES ('15', 'Can delete session', '5', 'delete_session');
INSERT INTO "auth_permission" VALUES ('16', 'Can add user', '6', 'add_bankuser');
INSERT INTO "auth_permission" VALUES ('17', 'Can change user', '6', 'change_bankuser');
INSERT INTO "auth_permission" VALUES ('18', 'Can delete user', '6', 'delete_bankuser');
INSERT INTO "auth_permission" VALUES ('19', 'Can add account', '7', 'add_account');
INSERT INTO "auth_permission" VALUES ('20', 'Can change account', '7', 'change_account');
INSERT INTO "auth_permission" VALUES ('21', 'Can delete account', '7', 'delete_account');
INSERT INTO "auth_permission" VALUES ('22', 'Can add transaction', '8', 'add_transaction');
INSERT INTO "auth_permission" VALUES ('23', 'Can change transaction', '8', 'change_transaction');
INSERT INTO "auth_permission" VALUES ('24', 'Can delete transaction', '8', 'delete_transaction');
COMMIT;

-- ----------------------------
--  Table structure for accounts_bankuser_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS "accounts_bankuser_user_permissions" CASCADE;
CREATE TABLE "accounts_bankuser_user_permissions" (
	"id" int4 NOT NULL DEFAULT nextval('accounts_bankuser_user_permissions_id_seq'::regclass),
	"bankuser_id" int4 NOT NULL,
	"permission_id" int4 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "accounts_bankuser_user_permissions" OWNER TO "stay-awake";

-- ----------------------------
--  Table structure for accounts_bankuser
-- ----------------------------
DROP TABLE IF EXISTS "accounts_bankuser" CASCADE;
CREATE TABLE "accounts_bankuser" (
	"id" int4 NOT NULL DEFAULT nextval('accounts_bankuser_id_seq'::regclass),
	"password" varchar(128) NOT NULL COLLATE "default",
	"last_login" timestamp(6) WITH TIME ZONE NOT NULL,
	"is_superuser" bool NOT NULL,
	"username" varchar(30) NOT NULL COLLATE "default",
	"first_name" varchar(30) NOT NULL COLLATE "default",
	"last_name" varchar(30) NOT NULL COLLATE "default",
	"email" varchar(75) NOT NULL COLLATE "default",
	"is_staff" bool NOT NULL,
	"is_active" bool NOT NULL,
	"date_joined" timestamp(6) WITH TIME ZONE NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "accounts_bankuser" OWNER TO "stay-awake";

-- ----------------------------
--  Records of accounts_bankuser
-- ----------------------------
BEGIN;
INSERT INTO "accounts_bankuser" VALUES ('1', 'pbkdf2_sha256$15000$J9SQi8d5b3vB$ogOO1J2Vi7nLFSNu246xvPfwb4ndPlxNWJuxrmEC8wY=', '2015-02-19 09:35:29.865269+02', 't', 'admin', '', '', '', 't', 't', '2015-02-19 09:35:14.207798+02');
INSERT INTO "accounts_bankuser" VALUES ('3', 'pbkdf2_sha256$15000$9f3nAUIpa6km$whlI2EDcQKjSuSuqMq/bXKndTyAhPkm8SYMJ6QShxhc=', '2015-02-19 09:36:36+02', 'f', 'john', '', '', '', 't', 't', '2015-02-19 09:36:36+02');
INSERT INTO "accounts_bankuser" VALUES ('2', 'pbkdf2_sha256$15000$3WHZV9Yt25AK$NZAPFnaByhoFEXNofl0mkxJQQZiHy6CwFVj9Jj9c5cE=', '2015-02-19 10:46:02.178448+02', 'f', 'mark', '', '', '', 't', 't', '2015-02-19 09:36:14+02');
COMMIT;

-- ----------------------------
--  Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS "django_admin_log" CASCADE;
CREATE TABLE "django_admin_log" (
	"id" int4 NOT NULL DEFAULT nextval('django_admin_log_id_seq'::regclass),
	"action_time" timestamp(6) WITH TIME ZONE NOT NULL,
	"object_id" text COLLATE "default",
	"object_repr" varchar(200) NOT NULL COLLATE "default",
	"action_flag" int2 NOT NULL,
	"change_message" text NOT NULL COLLATE "default",
	"content_type_id" int4,
	"user_id" int4 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "django_admin_log" OWNER TO "stay-awake";

-- ----------------------------
--  Records of django_admin_log
-- ----------------------------
BEGIN;
INSERT INTO "django_admin_log" VALUES ('1', '2015-02-19 09:36:06.609759+02', '1', 'general', '1', '', '3', '1');
INSERT INTO "django_admin_log" VALUES ('2', '2015-02-19 09:36:35.121977+02', '2', 'mark', '1', '', '6', '1');
INSERT INTO "django_admin_log" VALUES ('3', '2015-02-19 09:36:48.468322+02', '3', 'john', '1', '', '6', '1');
INSERT INTO "django_admin_log" VALUES ('4', '2015-02-19 09:37:11.549945+02', '1', 'Internet Payment Account', '1', '', '7', '1');
INSERT INTO "django_admin_log" VALUES ('5', '2015-02-19 09:37:30.65812+02', '2', 'Income Account', '1', '', '7', '1');
INSERT INTO "django_admin_log" VALUES ('6', '2015-02-19 09:37:44.434997+02', '1', '$0.01 at 07:37 19-02-2015', '1', '', '8', '1');
INSERT INTO "django_admin_log" VALUES ('7', '2015-02-19 09:37:53.817387+02', '1', '$2.31 at 07:37 19-02-2015', '2', 'Changed amount.', '8', '1');
INSERT INTO "django_admin_log" VALUES ('8', '2015-02-19 09:38:06.495901+02', '2', '$119.40 at 07:38 19-02-2015', '1', '', '8', '1');
INSERT INTO "django_admin_log" VALUES ('9', '2015-02-19 09:38:37.967055+02', '3', '$1.22 at 07:38 19-02-2015', '1', '', '8', '1');
INSERT INTO "django_admin_log" VALUES ('10', '2015-02-19 09:38:45.424534+02', '4', '$3.02 at 07:38 19-02-2015', '1', '', '8', '1');
INSERT INTO "django_admin_log" VALUES ('11', '2015-02-19 09:38:49.310639+02', '5', '$3 at 07:38 19-02-2015', '1', '', '8', '1');
INSERT INTO "django_admin_log" VALUES ('12', '2015-02-19 09:38:54.891543+02', '6', '$-2 at 07:38 19-02-2015', '1', '', '8', '1');
INSERT INTO "django_admin_log" VALUES ('13', '2015-02-19 10:51:32.963701+02', '7', '$335 at 08:51 19-02-2015', '1', '', '8', '2');
INSERT INTO "django_admin_log" VALUES ('14', '2015-02-19 10:51:41.815451+02', '8', '$12.22 at 08:51 19-02-2015', '1', '', '8', '2');
INSERT INTO "django_admin_log" VALUES ('15', '2015-02-19 10:51:50.907938+02', '9', '$12 at 08:51 19-02-2015', '1', '', '8', '2');
COMMIT;

-- ----------------------------
--  Table structure for accounts_account
-- ----------------------------
DROP TABLE IF EXISTS "accounts_account" CASCADE;
CREATE TABLE "accounts_account" (
	"id" int4 NOT NULL DEFAULT nextval('accounts_account_id_seq'::regclass),
	"title" varchar(255) NOT NULL COLLATE "default",
	"user_id" int4 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "accounts_account" OWNER TO "stay-awake";

-- ----------------------------
--  Records of accounts_account
-- ----------------------------
BEGIN;
INSERT INTO "accounts_account" VALUES ('1', 'Internet Payment Account', '2');
INSERT INTO "accounts_account" VALUES ('2', 'Income Account', '3');
COMMIT;

-- ----------------------------
--  Table structure for accounts_transaction
-- ----------------------------
DROP TABLE IF EXISTS "accounts_transaction" CASCADE;
CREATE TABLE "accounts_transaction" (
	"id" int4 NOT NULL DEFAULT nextval('accounts_transaction_id_seq'::regclass),
	"amount" numeric(10,2) NOT NULL,
	"date" timestamp(6) WITH TIME ZONE NOT NULL,
	"accpunt_id" int4 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "accounts_transaction" OWNER TO "stay-awake";

-- ----------------------------
--  Records of accounts_transaction
-- ----------------------------
BEGIN;
INSERT INTO "accounts_transaction" VALUES ('1', '2.31', '2015-02-19 09:37:44.431586+02', '1');
INSERT INTO "accounts_transaction" VALUES ('2', '119.40', '2015-02-19 09:38:06.49402+02', '2');
INSERT INTO "accounts_transaction" VALUES ('3', '1.22', '2015-02-19 09:38:37.965085+02', '1');
INSERT INTO "accounts_transaction" VALUES ('4', '3.02', '2015-02-19 09:38:45.423038+02', '2');
INSERT INTO "accounts_transaction" VALUES ('5', '3.00', '2015-02-19 09:38:49.309057+02', '2');
INSERT INTO "accounts_transaction" VALUES ('6', '-2.00', '2015-02-19 09:38:54.889954+02', '1');
INSERT INTO "accounts_transaction" VALUES ('7', '335.00', '2015-02-19 10:51:32.849892+02', '1');
INSERT INTO "accounts_transaction" VALUES ('8', '12.22', '2015-02-19 10:51:41.814195+02', '1');
INSERT INTO "accounts_transaction" VALUES ('9', '12.00', '2015-02-19 10:51:50.905554+02', '2');
COMMIT;

-- ----------------------------
--  Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS "django_session" CASCADE;
CREATE TABLE "django_session" (
	"session_key" varchar(40) NOT NULL COLLATE "default",
	"session_data" text NOT NULL COLLATE "default",
	"expire_date" timestamp(6) WITH TIME ZONE NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "django_session" OWNER TO "stay-awake";

-- ----------------------------
--  Records of django_session
-- ----------------------------
BEGIN;
INSERT INTO "django_session" VALUES ('jo1emr7dkfcw87udmj2ynqbrfn7bjugi', 'ZmFlZTk1MzE3YTIyYzQ1MTAwMDA5MWNiZTk3YWZjZjQxOGZmMTE0Mjp7Il9hdXRoX3VzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaGFzaCI6IjRmYTRiYzlhYWMyMmU5ZTQzYzgwNmUyNGVmMDFlM2E2MTI2MDYxMWEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9', '2015-03-05 10:46:02.180988+02');
COMMIT;


-- ----------------------------
--  Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "accounts_account_id_seq" RESTART 3 OWNED BY "accounts_account"."id";
ALTER SEQUENCE "accounts_bankuser_groups_id_seq" RESTART 3 OWNED BY "accounts_bankuser_groups"."id";
ALTER SEQUENCE "accounts_bankuser_id_seq" RESTART 4 OWNED BY "accounts_bankuser"."id";
ALTER SEQUENCE "accounts_bankuser_user_permissions_id_seq" RESTART 2 OWNED BY "accounts_bankuser_user_permissions"."id";
ALTER SEQUENCE "accounts_transaction_id_seq" RESTART 10 OWNED BY "accounts_transaction"."id";
ALTER SEQUENCE "auth_group_id_seq" RESTART 2 OWNED BY "auth_group"."id";
ALTER SEQUENCE "auth_group_permissions_id_seq" RESTART 3 OWNED BY "auth_group_permissions"."id";
ALTER SEQUENCE "auth_permission_id_seq" RESTART 25 OWNED BY "auth_permission"."id";
ALTER SEQUENCE "django_admin_log_id_seq" RESTART 16 OWNED BY "django_admin_log"."id";
ALTER SEQUENCE "django_content_type_id_seq" RESTART 9 OWNED BY "django_content_type"."id";
ALTER SEQUENCE "django_migrations_id_seq" RESTART 6 OWNED BY "django_migrations"."id";
-- ----------------------------
--  Primary key structure for table django_migrations
-- ----------------------------
ALTER TABLE "django_migrations" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table django_content_type
-- ----------------------------
ALTER TABLE "django_content_type" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table django_content_type
-- ----------------------------
ALTER TABLE "django_content_type" ADD CONSTRAINT "django_content_type_app_label_50be4f2068a8f5f0_uniq" UNIQUE ("app_label","model") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "auth_group_permissions" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "auth_group_permissions" ADD CONSTRAINT "auth_group_permissions_group_id_permission_id_key" UNIQUE ("group_id","permission_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table auth_group_permissions
-- ----------------------------
CREATE INDEX  "auth_group_permissions_0e939a4f" ON "auth_group_permissions" USING btree(group_id ASC NULLS LAST);
CREATE INDEX  "auth_group_permissions_8373b171" ON "auth_group_permissions" USING btree(permission_id ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table auth_group
-- ----------------------------
ALTER TABLE "auth_group" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table auth_group
-- ----------------------------
ALTER TABLE "auth_group" ADD CONSTRAINT "auth_group_name_key" UNIQUE ("name") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table auth_group
-- ----------------------------
CREATE INDEX  "auth_group_name_7cea5a4bc4dc83a3_like" ON "auth_group" USING btree("name" COLLATE "default" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table accounts_bankuser_groups
-- ----------------------------
ALTER TABLE "accounts_bankuser_groups" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table accounts_bankuser_groups
-- ----------------------------
ALTER TABLE "accounts_bankuser_groups" ADD CONSTRAINT "accounts_bankuser_groups_bankuser_id_group_id_key" UNIQUE ("bankuser_id","group_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table accounts_bankuser_groups
-- ----------------------------
CREATE INDEX  "accounts_bankuser_groups_0e939a4f" ON "accounts_bankuser_groups" USING btree(group_id ASC NULLS LAST);
CREATE INDEX  "accounts_bankuser_groups_9253674b" ON "accounts_bankuser_groups" USING btree(bankuser_id ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table auth_permission
-- ----------------------------
ALTER TABLE "auth_permission" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table auth_permission
-- ----------------------------
ALTER TABLE "auth_permission" ADD CONSTRAINT "auth_permission_content_type_id_codename_key" UNIQUE ("content_type_id","codename") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table auth_permission
-- ----------------------------
CREATE INDEX  "auth_permission_417f1b1c" ON "auth_permission" USING btree(content_type_id ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table accounts_bankuser_user_permissions
-- ----------------------------
ALTER TABLE "accounts_bankuser_user_permissions" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table accounts_bankuser_user_permissions
-- ----------------------------
ALTER TABLE "accounts_bankuser_user_permissions" ADD CONSTRAINT "accounts_bankuser_user_permission_bankuser_id_permission_id_key" UNIQUE ("bankuser_id","permission_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table accounts_bankuser_user_permissions
-- ----------------------------
CREATE INDEX  "accounts_bankuser_user_permissions_8373b171" ON "accounts_bankuser_user_permissions" USING btree(permission_id ASC NULLS LAST);
CREATE INDEX  "accounts_bankuser_user_permissions_9253674b" ON "accounts_bankuser_user_permissions" USING btree(bankuser_id ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table accounts_bankuser
-- ----------------------------
ALTER TABLE "accounts_bankuser" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table accounts_bankuser
-- ----------------------------
ALTER TABLE "accounts_bankuser" ADD CONSTRAINT "accounts_bankuser_username_key" UNIQUE ("username") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table accounts_bankuser
-- ----------------------------
CREATE INDEX  "accounts_bankuser_username_76c7dceb6d009b7f_like" ON "accounts_bankuser" USING btree(username COLLATE "default" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table django_admin_log
-- ----------------------------
ALTER TABLE "django_admin_log" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Checks structure for table django_admin_log
-- ----------------------------
ALTER TABLE "django_admin_log" ADD CONSTRAINT "django_admin_log_action_flag_check" CHECK ((action_flag >= 0)) NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table django_admin_log
-- ----------------------------
CREATE INDEX  "django_admin_log_417f1b1c" ON "django_admin_log" USING btree(content_type_id ASC NULLS LAST);
CREATE INDEX  "django_admin_log_e8701ad4" ON "django_admin_log" USING btree(user_id ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table accounts_account
-- ----------------------------
ALTER TABLE "accounts_account" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table accounts_account
-- ----------------------------
CREATE INDEX  "accounts_account_e8701ad4" ON "accounts_account" USING btree(user_id ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table accounts_transaction
-- ----------------------------
ALTER TABLE "accounts_transaction" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table accounts_transaction
-- ----------------------------
CREATE INDEX  "accounts_transaction_c0789811" ON "accounts_transaction" USING btree(accpunt_id ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table django_session
-- ----------------------------
ALTER TABLE "django_session" ADD PRIMARY KEY ("session_key") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table django_session
-- ----------------------------
CREATE INDEX  "django_session_de54fa62" ON "django_session" USING btree(expire_date ASC NULLS LAST);
CREATE INDEX  "django_session_session_key_3709ee19942b9a4c_like" ON "django_session" USING btree(session_key COLLATE "default" ASC NULLS LAST);

-- ----------------------------
--  Foreign keys structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "auth_group_permissions" ADD CONSTRAINT "auth_group_permissio_group_id_1fa81132a0897dc5_fk_auth_group_id" FOREIGN KEY ("group_id") REFERENCES "auth_group" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;
ALTER TABLE "auth_group_permissions" ADD CONSTRAINT "auth_group__permission_id_9ef4461332993c6_fk_auth_permission_id" FOREIGN KEY ("permission_id") REFERENCES "auth_permission" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;

-- ----------------------------
--  Foreign keys structure for table accounts_bankuser_groups
-- ----------------------------
ALTER TABLE "accounts_bankuser_groups" ADD CONSTRAINT "accounts_b_bankuser_id_7f5b250f9de065a5_fk_accounts_bankuser_id" FOREIGN KEY ("bankuser_id") REFERENCES "accounts_bankuser" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;
ALTER TABLE "accounts_bankuser_groups" ADD CONSTRAINT "accounts_bankuser_gr_group_id_7fd5617e0b852e90_fk_auth_group_id" FOREIGN KEY ("group_id") REFERENCES "auth_group" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;

-- ----------------------------
--  Foreign keys structure for table auth_permission
-- ----------------------------
ALTER TABLE "auth_permission" ADD CONSTRAINT "auth_content_type_id_57240c1872888fa6_fk_django_content_type_id" FOREIGN KEY ("content_type_id") REFERENCES "django_content_type" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;

-- ----------------------------
--  Foreign keys structure for table accounts_bankuser_user_permissions
-- ----------------------------
ALTER TABLE "accounts_bankuser_user_permissions" ADD CONSTRAINT "accounts_b_bankuser_id_15c5270a10adeb40_fk_accounts_bankuser_id" FOREIGN KEY ("bankuser_id") REFERENCES "accounts_bankuser" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;
ALTER TABLE "accounts_bankuser_user_permissions" ADD CONSTRAINT "accounts_b_permission_id_792467a67071db60_fk_auth_permission_id" FOREIGN KEY ("permission_id") REFERENCES "auth_permission" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;

-- ----------------------------
--  Foreign keys structure for table django_admin_log
-- ----------------------------
ALTER TABLE "django_admin_log" ADD CONSTRAINT "djang_content_type_id_e8c3d4a2b5bf196_fk_django_content_type_id" FOREIGN KEY ("content_type_id") REFERENCES "django_content_type" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;
ALTER TABLE "django_admin_log" ADD CONSTRAINT "django_admin_l_user_id_1d4c5856003d5a80_fk_accounts_bankuser_id" FOREIGN KEY ("user_id") REFERENCES "accounts_bankuser" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;

-- ----------------------------
--  Foreign keys structure for table accounts_account
-- ----------------------------
ALTER TABLE "accounts_account" ADD CONSTRAINT "accounts_accoun_user_id_fcda0b5b5ee2981_fk_accounts_bankuser_id" FOREIGN KEY ("user_id") REFERENCES "accounts_bankuser" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;

-- ----------------------------
--  Foreign keys structure for table accounts_transaction
-- ----------------------------
ALTER TABLE "accounts_transaction" ADD CONSTRAINT "accounts_tra_accpunt_id_153bf43f6e8235f6_fk_accounts_account_id" FOREIGN KEY ("accpunt_id") REFERENCES "accounts_account" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;

