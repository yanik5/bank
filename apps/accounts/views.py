# -*- encoding: utf-8 -*-
from json import dumps
import sqlalchemy as sa
from django.contrib.auth.decorators import login_required
from utils.decorators import render_to

from .models import BankUser, Account, Transaction


def to_json(model):
    """ Returns a JSON representation of an SQLAlchemy-backed object.
        Beaware (!) Decimal (Transaction.amount) is not JSON serializable
    """
    json = {}
    json['fields'] = {}
    json['pk'] = getattr(model, 'id')

    for col in model._sa_class_manager.mapper.mapped_table.columns:
        json['fields'][col.name] = getattr(model, col.name)

    return dumps([json])


@login_required
@render_to("accounts/single.html")
def single(request):
    user = BankUser.sa.query().filter(BankUser.sa.username == request.user.username)[0]

    # Hardcode (gettong first ) for now, later i will switch for GET varieble
    acc = Account.sa.query().filter(Account.sa.user == user)[0]

    # Typo in models - "accpunt". Change later.
    trans = Transaction.sa.query().filter(Transaction.sa.accpunt == acc)

    return dict({"username": user.username, "account": acc.title, "trans": trans})

