# -*- encoding: utf-8 -*-
from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^$', 'apps.accounts.views.single', name='single_account'),
)
