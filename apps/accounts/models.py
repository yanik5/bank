# -*- encoding: utf-8 -*-
from json import dumps

from django.db import models
from django.contrib.auth.models import AbstractUser


class BankUser(AbstractUser):
    pass


class Account(models.Model):
    title = models.CharField(max_length=255)
    user = models.ForeignKey(BankUser)

    def __str__(self):
        return self.title


class Transaction(models.Model):
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)
    # typo(!), change later
    accpunt = models.ForeignKey(Account)


    def __str__(self):
        return "${0} at {1}".format(self.amount, self.date.strftime('%H:%M %d-%m-%Y'))
