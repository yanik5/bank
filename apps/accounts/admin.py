# -*- encoding: utf-8 -*-
from django.contrib import admin
from .models import Transaction, Account, BankUser


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    pass


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(BankUser)
class BankUserAdmin(admin.ModelAdmin):
    pass